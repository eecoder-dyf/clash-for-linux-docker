[TOC]

# 项目介绍

此项目是通过使用开源项目[clash(已跑路)](https://github.com/Dreamacro/clash)作为核心程序，再结合脚本实现简单的代理功能，并添加了Docker版本方便无root权限的用户使用，请注意一台服务器只能运行一个该docker，使用前请仔细检查冲突。

主要是为了解决我们在服务器上下载GitHub等一些国外资源速度慢的问题。

由于作者已经跑路，当前为eecoder-dyf进行备份，若有侵犯您的权利，请提交issues我会看到并删除仓库，原项目说明文件为[Readme_old.md](Readme_old.md)

# Docker版本使用方法
## 准备
修改`.env`文件的`CLASH_URL`为订阅链接，`CLASH_SECRET`为一自定义简单密码如：123
## 搭建
```bash
cd Docker
docker-compose build
docker-compose up -d
```
## 配置节点（如果默认节点可用的话可以跳过）

查看`CLASH_SECRET`:

```bash
cat conf/config.yaml |grep secret:
```

浏览器打开`http://服务器ip:9090/ui` 或 [http://clash.razord.top/#/proxies](http://clash.razord.top/#/proxies)

**注：**若机器SSH时无法直连需要跳板机才能连接，则需要转发9090端口到本地，（直连服务器可选）。此时ip填127.0.0.1即可

第一种打开后输入密钥`CLASH_SECRET`，最终界面如图

![yacd](README.assets/yacd.png)

第二种打开后填写服务器ip和上面的`CLASH_SECRET`作为密钥，最终界面如图

![razord](README.assets/razord.png)

## 终端使用代理
在宿主机使用（不要在docker内使用）
```bash
source ./proxy.sh
proxy_on # 开启代理
proxy_off # 关闭代理
```

如果在校内，服务器ip地址为10.xxx.xxx.xxx，可以互通代理：

1. 在配置网页的设置(Config)中，打开Allow LAN（允许来自局域网的连接）

2. 将`proxy.sh`复制到需要开启代理的服务器，所有`127.0.0.1`替换为开启了clash-docker的服务器的ip地址:

   ```bash
   sed -i 's/127.0.0.1/10.xxx.xxx.xxx/g' ./proxy.sh
   ```
* 同理，如果想在同一台机器的其他docker内连接docker版的clash，则不应使用127.0.0.1，可用ifconfig查看docker0的ip: `ifconfig docker0`， 然后用`sed`命令将127.0.0.1替换成docker0的ip

然后就可以执行`proxy on`和`proxy_off`了

## Windows直接使用docker设置的代理
在 **设置-网络和Internet-代理-手动设置代理** 使用代理服务器设置如下图

ip填上服务器ip(如果是本地的docker就直接写成127.0.0.1),端口设为7890

绕过代理的规则：
```
localhost;127.*;10.*;172.16.*;172.17.*;172.18.*;172.19.*;172.20.*;172.21.*;172.22.*;172.23.*;172.24.*;172.25.*;172.26.*;172.27.*;172.28.*;172.29.*;172.30.*;172.31.*;192.168.*;cn
```
![Winodws](README.assets/windows.png)

## 测试代理是否可用

```bash
curl -vvvk https://www.google.com --connect-timeout 6
```

